#include "struct.h"

Figure *newFigure()
{
    Figure *fig = (Figure*)malloc(sizeof(Figure)); /*checar se está correto*/

    fig->id = 0;

    fig->radius = 0;
    fig->width = 0;
    fig->height = 0;

    fig->x = 0;
    fig->y = 0;

    fig->perColor = NULL; /*cor da borda*/
    fig->fillColor = NULL; /*cor do preenchimento*/

    fig->xcenter = 0;
    fig->ycenter = 0;

    return fig;
}

void freeFigures(Figure **fig)
{
  
  free((*fig)->perColor);
  free((*fig)->fillColor);

  free(*fig);
  *fig = NULL;
}

Arguments *newArguments()
{
  Arguments *arg = (Arguments*)malloc(sizeof(Arguments));
  
  arg->path = NULL;
  arg->directory = NULL;
  arg->fileName = NULL;
  arg->filePosition = NULL;
  arg->mainFile = NULL;
  arg->mainTXT = NULL;
  arg->svgA = NULL;
  arg->svgO = NULL;
  return arg;
}

void freeArguments(Arguments **arg)
{
  free((*arg)->path);
  free((*arg)->directory);
  free((*arg)->filePosition);
  free((*arg)->fileName);
  free((*arg)->mainFile);
  free((*arg)->mainFile );
  free((*arg)->mainTXT); 
  free((*arg)->svgA); 
  free((*arg)->svgO); 

  free(*arg);
  *arg = NULL;
}

ID *newID()
{
  ID *idFig =(ID*)malloc(sizeof(ID));
  
  idFig->id = 0;
  idFig->prox = NULL;

  return idFig;
}

void freeIDs(ID **id)
{
  free(*id);
}
