#include "comandos.h"

int changeMax(int figMax,char* instruction,FILE* file,Figure*** figures)
{
    fscanf(file,"%s",instruction);
    figMax = atoi(instruction);
    *figures = (Figure**)realloc(*figures,figMax);
    return figMax;
}

void alocarMemoria(char** var,int caract)
{
	*var = (char*)calloc(caract+1, sizeof(char));
}

void creatingCircle(Figure** figures,char* instruction,FILE* file,FILE* file2,ID **inicio,int figQTD)
{
    int id = atoi(instruction),i;
    double n;
    /*double xmax=1000100010001000,ymax=10001000100010001000;*/
    /*ID *figid = *inicio;
    
    for(i=0;i < figQTD;i++)   
    {
        if(figid->prox != NULL)
        {
            figid = figid->prox;
        }
    }

    figid->id = id-1;
    figid->prox = newID();
*/
    figures[id-1] = newFigure();

    fscanf(file,"%s",instruction);
    alocarMemoria(&figures[id-1]->perColor,strlen(instruction));
    strcpy(figures[id-1]->perColor,instruction);

    fscanf(file,"%s",instruction);
    alocarMemoria(&figures[id-1]->fillColor,strlen(instruction));
    strcpy(figures[id-1]->fillColor,instruction);
    fscanf(file,"%s",instruction);
    n = atof(instruction);
    figures[id-1]->radius = n;

    fscanf(file,"%s",instruction);
    n = atof(instruction);
    figures[id-1]->x = n;

    fscanf(file,"%s",instruction);
    n = atof(instruction);
    figures[id-1]->y = n;

    figures[id-1]->id = id;
    figures[id-1]->xcenter = figures[id-1]->x;
    figures[id-1]->ycenter = figures[id-1]->y;

 /*   if(figures[id-1]->x + figures[id-1]->radius > xmax || figures[id-1]->y + figures[id-1]->radius > ymax)
    {
        freeFigures(&figures[id-1]);
        printf("X ou Y estão acima do limite");
    }
*/
    insertingOnSVG(figures,id-1,file2);
}

void creatingRect(Figure** figures,char* instruction,FILE* file,FILE *file2,ID **inicio,int figQTD)
{
    int id = atoi(instruction),i;
    float n;
   /* int xmax=1000,ymax=1000;*/
    /*ID *figid = *inicio;
    
    for(i=0;i < figQTD;i++)   
    {
        if(figid->prox != NULL)
        {
            figid = figid->prox;
        }
    }

    figid->id = id-1;
    figid->prox = newID();
*/
    figures[id-1] = newFigure();

    fscanf(file,"%s",instruction);
    alocarMemoria(&figures[id-1]->perColor,strlen(instruction));
    strcpy(figures[id-1]->perColor,instruction);

    fscanf(file,"%s",instruction);
    alocarMemoria(&figures[id-1]->fillColor,strlen(instruction));
    strcpy(figures[id-1]->fillColor,instruction);

    fscanf(file,"%s",instruction);
    n = atof(instruction);
    figures[id-1]->width = n;

    fscanf(file,"%s",instruction);
    n = atof(instruction);
    figures[id-1]->height = n;

    fscanf(file,"%s",instruction);
    n = atof(instruction);
    figures[id-1]->x = n;


    fscanf(file,"%s",instruction);
    n = atof(instruction);
    figures[id-1]->y = n;

    figures[id-1]->id = id;
    figures[id-1]->xcenter = figures[id-1]->x + (figures[id-1]->width/2);
    figures[id-1]->ycenter = figures[id-1]->y + (figures[id-1]->height/2);

   /* if(figures[id-1]->x + figures[id-1]->width > xmax || figures[id-1]->y + figures[id-1]->height > ymax)
    {
        freeFigures(&figures[id-1]);
        printf("X ou Y estão acima do limite");
    }*/
    insertingOnSVG(figures,id-1,file2);    

}
/*CONSERTAR*/
int overlap(int id1,int id2,Figure** fig)
{
    float dist,menor;
    /*inicializando o menor valor*/
    if(fig[id1]->radius!=0)
    {
        menor = fig[id1]->radius*fig[id1]->radius;
    }
    else
    {
        menor = fig[id2]->radius*fig[id2]->radius;
    }

    /*retangulo-retangulo*/
    if(fig[id1]->radius==0 && fig[id2]->radius == 0)
    {
        /*if (!((fig[id2]->x >= fig[id1]->x+fig[id1]->width &&
         fig[id2]->y >= fig[id1]->y+fig[id1]->height)||(fig[id1]->x >= fig[id2]->x+fig[id2]->width &&
         fig[id1]->y >= fig[id2]->y+fig[id2]->height)||(fig[id1]->x >= fig[id2]->x+fig[id2]->width &&
         fig[id2]->y >= fig[id1]->y+fig[id1]->height)||(fig[id2]->x >= fig[id1]->x+fig[id1]->width &&
         fig[id1]->y >= fig[id2]->y+fig[id2]->height)))*/

		/* ponto esquerda-cima*/
		if(fig[id1]->x >= fig[id2]->x 
		 && fig[id1]->x <= fig[id2]->x+fig[id2]->width
		 && fig[id1]->y >= fig[id2]->y 
		 && fig[id1]->y <= fig[id2]->y+fig[id2]->height){
		
			return 1;
		}
        else
        {
			/* ponto direita cima*/
			if(fig[id1]->x+fig[id1]->width >= fig[id2]->x 
			&& fig[id1]->x+fig[id1]->width <= fig[id2]->x+fig[id2]->width
			&& fig[id1]->y >= fig[id2]->y 
			&& fig[id1]->y <= fig[id2]->y+fig[id2]->height)
            {
			
				return 1;	
	    	}
            else
            {
				/* ponto esquerda baixo*/
				if(fig[id1]->x >= fig[id2]->x 
				&& fig[id1]->x <= fig[id2]->x+fig[id2]->width
				&& fig[id1]->y+fig[id1]->height >= fig[id2]->y 
				&& fig[id1]->y+fig[id1]->height <= fig[id2]->y+fig[id2]->height)
                {

					return 1;	
				}
                else
                {
					/* ponto direita baixo*/
					if(fig[id1]->x+fig[id1]->width >= fig[id2]->x 
					&& fig[id1]->x+fig[id1]->width <= fig[id2]->x+fig[id2]->width
					&& fig[id1]->y+fig[id1]->height >= fig[id2]->y 
					&& fig[id1]->y+fig[id1]->height <= fig[id2]->y+fig[id2]->height)
                    {
					
						return 1;
					}
                    else
                    {
						if(fig[id2]->x >= fig[id1]->x 
						&& fig[id2]->x <= fig[id1]->x+fig[id1]->width
						&& fig[id2]->y >= fig[id1]->y 
					    && fig[id2]->y <= fig[id1]->y+fig[id1]->height)
                        {
							return 1;
						}
                        else
                        {
							/* ponto direita cima*/
							if(fig[id2]->x+fig[id2]->width >= fig[id1]->x 
							&& fig[id2]->x+fig[id2]->width <= fig[id1]->x+fig[id1]->width
							&& fig[id1]->y >= fig[id1]->y 
							&& fig[id1]->y <= fig[id1]->y+fig[id1]->height)
                            {
							
								return 1;	
							}
                            else
                            {
								/* ponto esquerda baixo*/
								if(fig[id2]->x >= fig[id1]->x 
								&& fig[id2]->x <= fig[id1]->x+fig[id1]->width
								&& fig[id2]->y+fig[id2]->height >= fig[id1]->y 
								&& fig[id2]->y+fig[id2]->height <= fig[id1]->y+fig[id1]->height)
                                {

									return 1;	
								}
                                else
                                {
									/* ponto direita baixo*/
									if(fig[id2]->x+fig[id2]->width >= fig[id1]->x 
									&& fig[id2]->x+fig[id2]->width <= fig[id1]->x+fig[id1]->width
									&& fig[id2]->y+fig[id2]->height >= fig[id1]->y 
									&& fig[id2]->y+fig[id2]->height <= fig[id1]->y+fig[id1]->height)
                                    {
									
										return 1;
									}
						        return 0;
								}
							}
						}
					}
				}
			}
		}
    }
    else
    {
    /*circulo-circulo*/ /*check*/
    if(fig[id1]->radius!=0 && fig[id2]->radius != 0)
    {
        dist = figDistance(id1,id2,fig);
        if (dist <= fig[id1]->radius + fig[id2]->radius)
        {
            return 2;
        }
        else
        {
            return 0;
        }
    
    }
    else
    {
    /*circulo-retangulo*/ /*check*/
    if(fig[id1]->radius!=0) /*se cond for verdadeira, id1 é um círculo, e id2 é um retangulo*/
    {
        dist = dotDistance(fig[id1]->x,fig[id1]->y,fig[id2]->x,fig[id2]->y);
        if(dist < menor)
        {
            menor = dist;
        }
        dist = dotDistance(fig[id1]->x,fig[id1]->y,fig[id2]->x + fig[id2]->width,fig[id2]->y);
        if(dist < menor)
        {
            menor = dist;
        }
        dist = dotDistance(fig[id1]->x,fig[id1]->y,fig[id2]->x,fig[id2]->y + fig[id2]->height);
        if(dist < menor)
        {
            menor = dist;
        }
        dist = dotDistance(fig[id1]->x,fig[id1]->y,fig[id2]->x + fig[id2]->width,fig[id2]->y + fig[id2]->height);
        if(dist < menor)
        {
            menor = dist;
        }

        dist = dotDistance(fig[id1]->x,fig[id1]->y,fig[id2]->xcenter,fig[id2]->ycenter);
        if(dist < menor)
        {
            menor = dist;
        }
        
        if(menor<fig[id1]->radius)
        {
            return 3;
        }
        else
        {
            return 0;
        }
    }
    else /*se cond for falsa, id2 é um círculo, e id1 é um retangulo*/
    {
        dist = dotDistance(fig[id2]->x,fig[id2]->y,fig[id1]->x,fig[id1]->y);
        if(dist < menor)
        {
            menor = dist;
        }
        dist = dotDistance(fig[id2]->x,fig[id2]->y,fig[id1]->x + fig[id1]->width,fig[id1]->y);
        if(dist < menor)
        {
            menor = dist;
        }
        dist = dotDistance(fig[id2]->x,fig[id2]->y,fig[id1]->x,fig[id1]->y + fig[id1]->height);
        if(dist < menor)
        {
            menor = dist;
        }
        dist = dotDistance(fig[id2]->x,fig[id2]->y,fig[id1]->x + fig[id1]->width,fig[id1]->y + fig[id1]->height);
        if(dist < menor)
        {
            menor = dist;
        }
        dist = dotDistance(fig[id2]->x,fig[id2]->y,fig[id1]->xcenter,fig[id1]->ycenter);
        if(dist < menor)
        {
            menor = dist;
        }
        if(menor<fig[id2]->radius)
        {
            return 3;
        }
        else
        {
            return 0;
        }
    }
    }
    }
}

int checkingDot(int id,Figure** fig,FILE *file,int caract)
{
    float *x,*y,*d;
    char *aux;
    
    aux = (char*)calloc(caract,sizeof(char));
    x = malloc(sizeof(float));
    y = malloc(sizeof(float));
    d = malloc(sizeof(float));
    fscanf(file,"%s",aux);
    *x = atof(aux);

    fscanf(file,"%s",aux);
    *y = atof(aux);
    
    if(fig[id]->radius == 0)
    {
        if( (fig[id]->x < *x && *x < fig[id]->x + fig[id]->width) && (fig[id]->y < *y && *y < fig[id]->y + fig[id]->height))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        *d = dotDistance(*x,*y,fig[id]->x,fig[id]->y);
        if(*d <= fig[id]->radius)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    free(aux);
    free(x);
    free(y);
    free(d);
}

void generateLine(FILE *svg,FILE *file,char *instruction,Figure** fig,Arguments* arg,ID **inicio,int figQTD)
{
    
    int i;
    int cont=0;
    ID *figid = *inicio;
    int* auxID;

    auxID = malloc(sizeof(int));
    *auxID = atoi(instruction)-1;

    fscanf(arg->mainFile,"%s",instruction);
    nameSVGA(arg,instruction);
    svg = fopen(arg->svgA,"w+");
    openingSVG(svg);

    /*while(figid->prox != NULL)
    {
        insertingOnSVG(fig,figid->id,svg);    
        figid = figid->prox;
        
    }*/
    i=cont=0;

    while(cont<figQTD)
    {
        if(!fig[i])
        {
            i++;
            printf("n\n");
            continue;
        }
        insertingOnSVG(fig,i,svg);
        i++;
        cont++;
    }
  
    figid = *inicio;
    drawLinesAB(fig,svg,*auxID,figQTD);

    closingSVG(svg);

}