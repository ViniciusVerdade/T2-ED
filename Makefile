COMPILER = gcc
CFLAGS = -lm -ansi -pedantic -I.

# Colocar aqui os arquivos que devem ser compilados

OBJ = main.o argumento.o funcoes.o comandos.o figuras.o struct.o 

%.o: %.c 
	$(COMPILER) -c -o $@ $< $(CFLAGS)

siguel: $(OBJ)
	$(COMPILER) -o $@ $^ $(CFLAGS)

debug: $(OBJ)
	$(COMPILER) -o siguel $^ $(CFLAGS) -g
.PHONY: clean
clean:
	rm $(OBJ)