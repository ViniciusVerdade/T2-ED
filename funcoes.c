#include "funcoes.h"

int caractLinha( FILE *fin )
{
    int c, count;

    count = 0;
    for( ;; )
    {
        c = fgetc( fin );
        if( c == EOF || c == '\n' )
            break;
        ++count;
    }
	fseek(fin,-(count+1),SEEK_CUR);
    return count;
}

int lerLinha(int caract,char** instruction, FILE* file)
{
	caract = caractLinha(file);
    alocarMemoria(instruction,caract);
    fscanf(file,"%s",*instruction);
	return caract;
}

void pularLinha(Arguments* var)
{
    if(!feof(var->mainFile))
    {
        fseek(var->mainFile,1,SEEK_CUR);
	}
}

float figDistance(int id1,int id2,Figure** fig)
{
    float d;
    if( fig[id1]->xcenter <= fig[id2]->xcenter)
    {
        if(fig[id1]->ycenter <= fig[id2]->ycenter)
        {
            d = pow( ( fig[id2]->ycenter - fig[id1]->ycenter )*( fig[id2]->ycenter - fig[id1]->ycenter ) + ( fig[id2]->xcenter - fig[id1]->xcenter )*( fig[id2]->xcenter - fig[id1]->xcenter ),0.5);
        }
        else
        {
            d = pow( ( fig[id1]->ycenter - fig[id2]->ycenter )*( fig[id1]->ycenter - fig[id2]->ycenter ) + ( fig[id2]->xcenter - fig[id1]->xcenter )*( fig[id2]->xcenter - fig[id1]->xcenter ),0.5);
        }
    }
    else
    {
        if(fig[id1]->ycenter <= fig[id2]->ycenter)
        {
            d = pow( ( fig[id2]->ycenter - fig[id1]->ycenter )*( fig[id2]->ycenter - fig[id1]->ycenter ) + ( fig[id1]->xcenter - fig[id2]->xcenter )*( fig[id1]->xcenter - fig[id2]->xcenter ) ,0.5);
        }
        else
        {
            d = pow( ( fig[id1]->ycenter - fig[id2]->ycenter )*( fig[id1]->ycenter - fig[id2]->ycenter ) + ( fig[id1]->xcenter - fig[id2]->xcenter )*( fig[id1]->xcenter - fig[id2]->xcenter ) ,0.5);
        }
    }
    return d;
}

float dotDistance(float x1,float y1,float x2,float y2)
{
    float d;
    
    if(x1 >= x2)
    {
        if(y1 >= y2)
        {
            d = pow( ( y1 - y2 )*( y1 - y2 ) + ( x1 - x2 )*( x1 - x2 ),0.5 );
        }
        else
        {
            d =  pow( ( y2 - y1 )*( y2 - y1 ) + ( x1 - x2 )* ( x1 - x2 ),0.5);
        }
    }
    else
    {
        if(y1 >= y2)
        {
            d =  pow( ( y1 - y2 )*( y1 - y2 ) + ( x2 - x1 )*( x2 - x1 ),0.5);
        }
        else
        {
            d =  pow( ( y2 - y1 )*( y2 - y1 ) + ( x2 - x1 )*( x2 - x1 ),0.5);
        }
    }

    return d;
}

void fileNameDefinition(Arguments** arg)
{
    char* aux;
    int i,j;
    /*Tratando do nome do arquivo txt*/
    alocarMemoria(&aux,strlen((*arg)->filePosition));
    strcpy(aux,(*arg)->filePosition);
    i = strlen(aux)-1;
    if(aux[i-3]=='.')
    {
        aux[i-2]='t';
        aux[i-1]='x';
        aux[i]='t';
    }
    alocarMemoria(&(*arg)->mainTXT,strlen(aux));
    strcpy((*arg)->mainTXT,aux);
    if(aux[i-3]=='.')
    {
        aux[i-2]='s';
        aux[i-1]='v';
        aux[i]='g';
    }
    alocarMemoria(&(*arg)->svgO,strlen(aux));
    strcpy((*arg)->svgO,aux);
}

void getLine(char* aux,FILE* file,int caract)
{
    fseek(file,-1,SEEK_CUR);
    fgets(aux,caract+1,file);
    fseek(file,-(caract+1),SEEK_CUR);
}

void initializingFigures(Figure **figures,int figmax)
{
    int i;
    for(i=0;i<figmax;i++)
    {
        figures[i] = NULL;
    }
}
void openingSVG(FILE* file)
{
    fprintf(file,"%s","<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"100000\" height=\"100000\">\n");/*Abrindo a imagem svg*/
}

float max(float a, float b) 
{
    return (a > b) ? a : b;
}
float min(float a, float b) 
{
    return (a < b) ? a : b;
}

void nameSVGA(Arguments *arg,char* instr)
{
    char* aux,*aux2;
    int i,j;
    alocarMemoria(&aux,strlen(arg->svgO)+strlen(instr)+2);
    alocarMemoria(&aux2,strlen(arg->svgO)+strlen(instr)+2);
    strcpy(aux2,arg->svgO);
    i = strlen(aux2)-1;
    j = i-4;
    aux2[j+1] = NULL;
    aux2[j+2] = NULL;
    aux2[j+3] = NULL;

    sprintf(aux, "%s-%s.svg", aux2, instr);
    alocarMemoria(&arg->svgA,strlen(aux));
    strcpy(arg->svgA,aux);
}