#include "argumento.h"
#include "struct.h"

Arguments *argumentos(int argc,const char* argv[] )
{
    int i,j,k;/* contadores*/
    int bol = 0;/*variavel binaria*/
    Arguments *argum = newArguments();
    /*todo IMPLEMENTAR A CORREÇÃO PARA CASO SEJA INSERIDO OU NÃO "/" NO FINAL DO PATH E DO DIRETÓRIO*/

    for(i=1 ; i < argc ; i++)
    {
        if( strcmp(argv[i] ,"-f") == 0) /*? TRABALHANDO O NOME DO ARQUIVO*/
        {
            j = i+1;

            argum->fileName =(char*)calloc(strlen(argv[j])+1,sizeof(char));
            
            strcpy(argum->fileName,argv[j]);
        }

        if( strcmp(argv[i] ,"-o") == 0)/*? TRABALHANDO O DIRETORIO EM QUE SERÁ CRIADO OS ARQUIVOS*/
        {
            j = i+1;

            argum->directory = (char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argum->directory, argv[j]);
        }
        
        if( strcmp(argv[i],"-e") == 0) /*todo IMPLEMENTAR */
        {
            j = i+1;
            bol = 1;

            argum->path = (char*)calloc(strlen(argv[j]+1),sizeof(char));
            strcpy(argum->path, argv[j]);
        }
    }
    if(bol == 1)
    {
        argum->filePosition = (char*)calloc(strlen(argum->path)+strlen(argum->fileName)+1,sizeof(char));
        strcpy(argum->filePosition,argum->path);
        strcat(argum->filePosition,argum->fileName);
    }
    else
    {
        argum->filePosition = (char*)calloc(strlen(argum->fileName)+1,sizeof(char));
        strcat(argum->filePosition,argum->fileName);
    }
    return argum;
}
