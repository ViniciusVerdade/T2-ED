#ifndef ESTRUTURA
#define ESTRUTURA

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct figur{
    int id;
    float radius;
    float width;
    float height;
    float x;
    float y;
    float xcenter;
    float ycenter;
    char *perColor; /*cor da borda*/
    char *fillColor; /*cor do preenchimento*/
};
typedef struct figur Figure;

struct Arg{
    char* path;
    char* directory;
    char* filePosition;
    char* fileName;
    FILE *mainFile;
    char* mainTXT;
    char* svgA;
    char* svgO;
};
typedef struct Arg Arguments;

struct indentification{
    int id;
    struct ID *prox;
};
typedef struct indentification ID;

/*talvez não enviar as cores devido a necessidade de checar a quantidade pro malloc*/
Figure *newFigure();
void freeFigures(Figure **fig);

Arguments *newArguments();
void freeArguments(Arguments **arg);

ID *newID();

void freeIDs(ID **id);


#endif