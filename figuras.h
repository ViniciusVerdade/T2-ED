#ifndef FIGURAS
#define FIGURAS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "comandos.h"
#include "funcoes.h"

void overlapSVGO(FILE *file, Figure** fig,int id1,int id2);

void insertingOnSVG(Figure** fig, int id,FILE *file);

void closingSVG(FILE *arqsvg);

void drawLinesAB(Figure** fig, FILE* svg,int auxID,int figQTD);

#endif