#ifndef FUNCOES
#define FUNCOES

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "comandos.h"

int caractLinha( FILE *fin );

int lerLinha(int caract,char** instruction, FILE* file);

void pularLinha(Arguments* var);

float figDistance(int id1,int id2,Figure** fig);

float dotDistance(float x1,float y1,float x2,float y2);

void fileNameDefinition(Arguments** arg);

void getLine(char* aux,FILE* file,int caract);

void initializingFigures(Figure **figures,int figmax);

float max(float a, float b);

float min(float a, float b);

void nameSVGA(Arguments *arg,char* instr);

#endif