#ifndef COMANDOS
#define COMANDOS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "funcoes.h"
#include "figuras.h"

int changeMax(int figMax,char* instruction,FILE* file,Figure*** figures);

void creatingCircle(Figure** figures,char* instruction,FILE* file,FILE* file2,ID **inicio,int figQTD);

void creatingRect(Figure** figures,char* instruction,FILE* file,FILE *file2,ID **inicio,int figQTD);

void alocarMemoria(char** var,int caract);

int overlap(int id1,int id2,Figure** fig);

int checkingDot(int id,Figure** fig,FILE *file,int caract);

void generateLine(FILE *svg,FILE *file,char *instruction,Figure** fig,Arguments* arg,ID **inicio,int figQTD);

#endif