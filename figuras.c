#include "figuras.h"

void overlapSVGO(FILE *file, Figure** fig,int id1,int id2)
{

    float x, y, h, w;

    if(fig[id1]->radius == 0)/*CASO VERDADE SERA UM RETANGULO*/
    {
        if(fig[id2]->radius == 0)/*CASO VERDADE, AS DUAS FIG SERÃO RETANGULOS*/
        {
            x = min(fig[id1]->x, fig[id2]->x);
            y = min(fig[id1]->y, fig[id2]->y);

            w = max(fig[id1]->x + fig[id1]->width, fig[id2]->x + fig[id2]->width);
            h = max(fig[id1]->y + fig[id1]->height, fig[id2]->y + fig[id2]->height);

        }
        else/*CASO VERDADE, FIG1 É RETANGULO E FIG2 É CIRCULO*/
        {
            x = min(fig[id1]->x, fig[id2]->x - fig[id2]->radius);
            y = min(fig[id1]->y, fig[id2]->y - fig[id2]->radius);

            w = max(fig[id1]->x + fig[id1]->width, fig[id2]->x + fig[id2]->radius);
            h = max(fig[id1]->y + fig[id1]->height, fig[id2]->y + fig[id2]->radius);

        }
    }
    else
    {
        if(fig[id2]->radius == 0)/*CASO VERDADE, FIG1 É CIRCULO E FIG2 É RETANGULO*/
        {
            x = min(fig[id2]->x, fig[id1]->x - fig[id1]->radius);
            y = min(fig[id2]->y, fig[id1]->y - fig[id1]->radius);

            w = max(fig[id2]->x + fig[id2]->width, fig[id1]->x + fig[id1]->radius);
            h = max(fig[id2]->y + fig[id2]->height, fig[id1]->y + fig[id1]->radius);
        }
        else/*CASO VERDADE, AS DUAS FIG SERÃO CIRCULOS*/
        {
            x = min(fig[id1]->x - fig[id1]->radius, fig[id2]->x - fig[id2]->radius);
            y = min(fig[id1]->y - fig[id1]->radius, fig[id2]->y - fig[id2]->radius);

            w = max(fig[id1]->x + fig[id1]->radius, fig[id2]->x + fig[id2]->radius);
            h = max(fig[id1]->y + fig[id1]->radius, fig[id2]->y + fig[id2]->radius);
        }
    }

    w-=x;
    h-=y;

    x-=1;
    y-=1;

    w += 2 * 1;
    h += 2 * 1;

fprintf(file,"%s%.1f%s%.1f%s%.1f%s%.1f%s","<rect x=\"",x,"\" y=\"",y,"\" width=\"",w,"\" height=\"",h,"\" stroke-dasharray=\"1, 1\" style=\"fill:transparent;stroke-width:1;stroke:purple;\" />\n"); /*printando um retangulo*/
fprintf(file,"%s%.1f%s%.1f%s","<text x=\"",x,"\" y=\"",y,"\" style=\"fill:purple;font-size:12.0px;font-family:sans-serif\">Sobrepõe</text>\n");

}

void insertingOnSVG(Figure** fig, int id,FILE *file)
{
    if(fig[id]->radius==0)
    {
        fprintf(file,"%s%.1f%s%.1f%s%.1f%s%.1f%s%s%s%s%s","<rect x=\"",fig[id]->x,"\" y=\"",fig[id]->y,"\" width=\"",fig[id]->width,"\" height=\"",fig[id]->height,"\" style=\"fill:",fig[id]->fillColor,";stroke-width:1;stroke:",fig[id]->perColor,";opacity:0.4;\" />\n"); /*printando um retangulo*/
    }
    else
    {
        fprintf(file,"%s%.1f%s%.1f%s%.1f%s%s%s%s%s","<circle cx=\"",fig[id]->x,"\" cy=\"",fig[id]->y,"\" r=\"",fig[id]->radius,"\" style=\"fill:",fig[id]->fillColor,";stroke-width:1;stroke:",fig[id]->perColor,";opacity:0.4;\" />\n");
    }
}

void closingSVG(FILE *arqsvg)
{
    fprintf(arqsvg,"%s","</svg>");
    fclose(arqsvg);
}

void drawLinesAB(Figure** fig, FILE* svg,int auxID,int figQTD)
{
    /*<line x1="120.0" y1="45.0" x2="171.0" y2="61.0" style="stroke:magenta;stroke-width:1" />
    <text x="155.5" y="53.0" style="fill:magenta;font-size:15.0px;font-family:sans-serif">53.5</text>*/
    int i=0;
    float dist;
    int cont=0;
    
    while(cont<figQTD)
    {
        if(!fig[i])
        {
            i++;
            printf("n\n");
            continue;
        }
        if(auxID != i)
        {
            dist = dotDistance(fig[auxID]->xcenter,fig[auxID]->ycenter,fig[i]->xcenter,fig[i]->ycenter);
            
            fprintf(svg,"<line x1=\"%.1f\" y1=\"%.1f\" x2=\"%.1f\" y2=\"%.1f\" style=\"stroke:%s;stroke-width:1\" />\n", /*PRINTANDO AS LINHAS*/
            fig[auxID]->xcenter,fig[auxID]->ycenter,fig[i]->xcenter,fig[i]->ycenter,fig[auxID]->perColor);

            fprintf(svg,"<text x=\"%.1f\" y=\"%.1f\" style=\"fill:%s;font-size:12.0px;font-family:sans-serif\">%.1f</text>\n",
            (fig[auxID]->xcenter+fig[i]->xcenter)/2+5,(fig[auxID]->ycenter+fig[i]->ycenter)/2+5,fig[auxID]->perColor,dist);
            
        }
        i++;
        cont++;
    }
    /*
    while(figID->prox != NULL)
    {
        if(auxID != figID->id)
        {
            i = dotDistance(fig[auxID]->xcenter,fig[auxID]->ycenter,fig[figID->id]->xcenter,fig[figID->id]->ycenter);
            
            fprintf(svg,"<line x1=\"%.1f\" y1=\"%.1f\" x2=\"%.1f\" y2=\"%.1f\" style=\"stroke:%s;stroke-width:1\" />\n", /*PRINTANDO AS LINHAS*/
    /*        fig[auxID]->xcenter,fig[auxID]->ycenter,fig[figID->id]->xcenter,fig[figID->id]->ycenter,fig[auxID]->perColor);

            fprintf(svg,"<text x=\"%.1f\" y=\"%.1f\" style=\"fill:%s;font-size:12.0px;font-family:sans-serif\">%.1f</text>\n",
            (fig[auxID]->xcenter+fig[figID->id]->xcenter)/2+5,(fig[auxID]->ycenter+fig[figID->id]->ycenter)/2+5,fig[auxID]->perColor,i);
            
        }
        figID = figID->prox;
    }
*/

}