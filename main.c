#include "struct.h"
#include "funcoes.h"
#include "figuras.h"
#include "comandos.h"
#include "argumento.h"

void main(int argc, const char* argv[] )
{

    Arguments *arg = argumentos(argc,argv); /*criando a struct ponteiro que passa as informações recebidas na invocação*/
    ID *figId = newID();
    ID *inicio = figId;

    char *instruction;

    int i,id1,id2,contador=0; /* total de lines no arquivo e contadores*/
    int caract; /*numero de caracteres na linha*/
    int figMax = 1000,figQt = 0,*valor,*valor2; /** VALOR MÁXIMO DE FIGURAS*/
    int help;
    float *distance;
            

    FILE* arqtxt;
    FILE* arqsvgo;
    FILE* arqsvga;

    Figure **figures =(Figure**)calloc(figMax,sizeof(Figure*));/* "vetor de figuras"*/
    Figure **figuresAUX;
    char c; /*auxiliar char*/
    char *aux; /*auxiliares para char ponteiro*/
    
    /*Utilizado apenas para diminuir o codigo, apagar dps*/
    {
        initializingFigures(figures,figMax);
        arg->mainFile = fopen(arg->filePosition,"r+"); /*tratando o arquivo .geo*/
        if(arg->mainFile == NULL){
            printf("Erro, nao foi possivel abrir o arquivo\n");
        }
        help = figMax;
        fileNameDefinition(&arg);
        arqtxt = fopen(arg->mainTXT,"w+");       
        arqsvgo = fopen(arg->svgO,"w+");
    }
    openingSVG(arqsvgo);
    while(!feof(arg->mainFile))
    {

        caract = lerLinha(caract, &instruction, arg->mainFile);
        
 
        if( strcmp(instruction,"nx") == 0) /*alterar número máximo de figuras possíveis*/
        {
            figMax = changeMax(figMax,instruction,arg->mainFile,&figures);
            if(figMax>help)
            {
                figuresAUX = (Figure**)calloc(figMax,sizeof(Figure*));

                i     = 0;
                contador = 0;

                while (contador < figQt) {
                    if (!figures[i]) {
                    i++;
                    continue;
                    }
                    figuresAUX[i] = figures[i];
                    contador++;
                    i++;
                }

                free(figures);
                figures = figuresAUX;
            }
            printf("CERTO NX\n");
        }

        if( strcmp(instruction,"c") == 0) /*criando círculo*/
        {
            if(figQt < figMax)
            {
                fscanf(arg->mainFile,"%s",instruction);
                creatingCircle(figures,instruction,arg->mainFile,arqsvgo,&figId,figQt);
                figQt++;
            }
            else
            {
                printf("\nJá há figuras o suficiente\n");
            }
        }

        if( strcmp(instruction,"r") == 0) /*criando retângulo*/
        {
            if(figQt < figMax)
            {
                fscanf(arg->mainFile,"%s",instruction);
                creatingRect(figures,instruction,arg->mainFile,arqsvgo,&figId,figQt);
                figQt++;
            }
            else
            {
                printf("\nJá há figuras o suficiente\n");
            }
        }
 /*CONSERTAR A PORRA DO OVERLAP!!!!*/
        if( strcmp(instruction,"o") == 0) /*alterar número máximo de figuras possíveis*/
        {
            alocarMemoria(&aux,caract);
            getLine(aux,arg->mainFile,caract);

            fscanf(arg->mainFile,"%s",instruction); /*arrumando a posição de instruction*/
            fscanf(arg->mainFile,"%s",instruction);
            id1 = atoi(instruction)-1;
            fscanf(arg->mainFile,"%s",instruction);
            id2 = atoi(instruction)-1;
            valor = malloc(sizeof (int));
            *valor = overlap(id1,id2,figures);
            if(*valor != 0)
            {
                strcat(aux,"\n");
                fprintf(arqtxt,"%s",aux);
                fprintf(arqtxt,"%s","SIM\n");
                overlapSVGO(arqsvgo,figures,id1,id2); 
            }
            else
            {
                strcat(aux,"\n");
                fprintf(arqtxt,"%s",aux);
                fprintf(arqtxt,"%s","NAO\n");
            }
            free(valor);
            free(aux);
        }

        if( strcmp(instruction,"i") == 0) /*se o ponto esta dentro da figura de id indicado*/
        {
            alocarMemoria(&aux,caract);
            getLine(aux,arg->mainFile,caract);

            fscanf(arg->mainFile,"%s",instruction);/*ajeitando a posição no arquivo*/
            fscanf(arg->mainFile,"%s",instruction);

            valor = malloc(sizeof(int));
            *valor = atoi(instruction);

            *valor = checkingDot((*valor)-1,figures,arg->mainFile,caract);
            if(*valor == 1)
            {
                strcat(aux,"\n");
                fprintf(arqtxt,"%s",aux);
                fprintf(arqtxt,"%s","SIM\n");
            }
            else
            {
                strcat(aux,"\n");
                fprintf(arqtxt,"%s",aux);
                fprintf(arqtxt,"%s","NAO\n");
            }
            free(valor);
            free(aux);
        }

        if( strcmp(instruction,"d") == 0) /*distanica entre dois pontos*/
        {
            alocarMemoria(&aux,caract);
            getLine(aux,arg->mainFile,caract);
            fscanf(arg->mainFile,"%s",instruction);/*ajeitando a posição no arquivo*/

            fscanf(arg->mainFile,"%s",instruction);
            valor = malloc(sizeof(int));
            *valor = atoi(instruction);
            
            fscanf(arg->mainFile,"%s",instruction);
            valor2 = malloc(sizeof(int));
            *valor2 = atoi(instruction);
            distance = malloc(sizeof(float));
            *distance = figDistance(*valor-1,*valor2-1,figures);

            strcat(aux,"\n");
            fprintf(arqtxt,"%s",aux);
            fprintf(arqtxt,"%.1f",*distance);
            fprintf(arqtxt,"%s","\n");

            free(distance);
            free(aux);
            free(valor);
            free(valor2);
        }

        if( strcmp(instruction,"a") == 0)
        {   
            fscanf(arg->mainFile,"%s",instruction);
            valor = malloc(sizeof(int));
            *valor = atoi(instruction);
            generateLine(arqsvga,arg->mainFile,instruction,figures,arg,&inicio,figQt);
        }
        pularLinha(arg);
        free(instruction);
    }
/*
    printf("\n\nTESTE DE ID's:\n");
    /**figId = *inicio;
    for(i = 0;i<figQt;i++  )
    {
        printf("%d\n",figId->id);
        figId = figId->prox;
    }
*/
    free(figures);
    fclose(arqtxt);
    closingSVG(arqsvgo);
    printf("\nCORRETO!\n");
}

